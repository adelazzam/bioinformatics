/*
 * Name here...
 * CSCI 4567 
 * 10/24/2011
 * 
 * 
 * Based on Prog. 2b)
 * "Write a program (PERL is suggested) to scan for ORF�s (with three forward reading frames, 
 * and three reverse reading frames on the complement strand). Introduce a cutoff for 
 * ORF�s > 500 bases, as in results described in Ch. 2. 
 * Use the 1st ATG heuristic to identify genes."
 * -"Machine-Learning based sequence analysis, bioinformatics & nanopore transduction detection"  
 * by Stephen Winters-Hilt 
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.BufferedInputStream;
//import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
//import java.io.IOException;




public class ReadExample {

	
	public static void main(String[] args) {
	
		
	//looking for atgcccactttcaccgctgcttgcccactga	
	
	//try-catch block to parse the genome DNA file	
	try {
	    BufferedReader in = new BufferedReader(new 
	    		FileReader("C:\\Users\\aazzam\\Desktop\\Fall_2011_Courses\\CSCI_BIOINFORMATICS\\sample.dna"));
	    String str;
	    String atg;
	    
	    String result;
	    
	    int i = 0;
	    int startpos = 0;
	    
	    int endpos = 0;
	    while (((str = in.readLine()) != null)){ // && (i<1000)) {
	    	
	    	startpos = str.lastIndexOf("atg" );
	    	
	    	//add 3 to get the full stop codon, tga
	    	endpos = str.lastIndexOf("tga" ) + 3;
	    	result = str.substring(startpos, endpos);
	    	
	    	System.out.println("\n" + "First ORF " + result + " /end FIRST ORF " + startpos + "\n");		    	
	    	System.out.println("\n" + "" + endpos + "\n");	
	    		    	
	    	System.out.print(str);
	    	
//	    	System.out.print(str.charAt(i));
	    	
	       //System.out.println(str.charAt(5));//str);// process(str);
	    	i++;
	    }
	    
	    in.close();
	    
	} catch (IOException e) {
	
	}

	
	
}}
